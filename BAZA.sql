-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 02, 2020 at 06:43 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php2t2`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(40) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`) VALUES
(1, 'Bela tehnika'),
(2, 'Mobilni telefoni'),
(5, 'dadasd'),
(6, '121212'),
(8, 'pera1');

-- --------------------------------------------------------

--
-- Table structure for table `film`
--

CREATE TABLE `film` (
  `id` int(11) NOT NULL,
  `naziv` varchar(40) NOT NULL,
  `opis` text NOT NULL,
  `slika` varchar(100) NOT NULL,
  `korisnik_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `film`
--

INSERT INTO `film` (`id`, `naziv`, `opis`, `slika`, `korisnik_id`) VALUES
(1, 'Interstellar', 'A team of explorers travel through a wormhole in space in an attempt to ensure humanity\'s survival. ', 'interstellar.jpg', 1),
(2, 'The Shawshank Redemption', 'Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency. ', 'shawshank.jpg', 1),
(3, 'asdasd', 'asdasd', 'asdasd', 1),
(4, 'Ime filma', 'Opis filma', 'images/1530571725.jpg', 1),
(5, 'Naziv', 'Opis', 'images/1530574245.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(255) NOT NULL,
  `src` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `productId` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `src`, `productId`) VALUES
(1, '1583170041_download.jpg', 6),
(2, '1583170519_stage (1).png', 7);

-- --------------------------------------------------------

--
-- Table structure for table `korisnik`
--

CREATE TABLE `korisnik` (
  `id` int(11) NOT NULL,
  `ime` varchar(20) NOT NULL,
  `prezime` varchar(20) NOT NULL,
  `email` varchar(60) NOT NULL,
  `lozinka` varchar(32) NOT NULL,
  `korisnicko_ime` varchar(20) NOT NULL,
  `uloga_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `korisnik`
--

INSERT INTO `korisnik` (`id`, `ime`, `prezime`, `email`, `lozinka`, `korisnicko_ime`, `uloga_id`) VALUES
(1, 'Pera', 'Peric', 'pera@gmail.com', 'd0aeeef9a9aeddbaa999b7b65101b3a1', 'pera', 2),
(2, 'Mika', 'Mikic', 'mika@gmail.com', 'd0aeeef9a9aeddbaa999b7b65101b3a1', 'mika', 3);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(255) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `categoryId` int(255) NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `description`, `categoryId`, `price`) VALUES
(1, 'Danijela', 'dasdasdasdasd', 2, '121212.00'),
(6, 'LUKA', 'dasdasdasdasd', 2, '121212.00'),
(7, 'DANIJELAPROBA', 'dasdasd', 2, '21121.00');

-- --------------------------------------------------------

--
-- Table structure for table `uloga`
--

CREATE TABLE `uloga` (
  `id` int(11) NOT NULL,
  `naziv` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `uloga`
--

INSERT INTO `uloga` (`id`, `naziv`) VALUES
(2, 'korisnik'),
(3, 'admin'),
(4, 'dadasd'),
(6, 'dadasddasdasd'),
(8, 'dadasddasdasddasdasd'),
(9, 'dasdasdasd'),
(10, 'dasd1212'),
(11, 'dasdasdasdas'),
(13, 'dadasdasd'),
(25, 'Uloga: 127.0.0.1'),
(28, 'Uloga: 127.0.0.1'),
(29, 'Uloga: 127.0.0.1'),
(30, 'Uloga: 127.0.0.1'),
(31, 'Uloga: 127.0.0.1'),
(32, 'Uloga: 127.0.0.1'),
(33, 'Uloga: 127.0.0.1'),
(34, 'Uloga: 127.0.0.1'),
(35, 'Uloga: 127.0.0.1'),
(36, 'Uloga: 127.0.0.1'),
(37, 'Uloga: 127.0.0.1'),
(38, 'Uloga: 127.0.0.1'),
(39, 'Uloga: 127.0.0.1'),
(40, 'Uloga: 127.0.0.1'),
(41, 'Uloga: 127.0.0.1'),
(42, 'Uloga: 127.0.0.1'),
(43, 'Uloga: 127.0.0.1'),
(44, 'Uloga: 127.0.0.1'),
(45, 'Uloga: 127.0.0.1'),
(46, 'Uloga: 127.0.0.1'),
(47, 'Uloga: 127.0.0.1'),
(48, 'Uloga: 127.0.0.1'),
(49, 'Uloga: 127.0.0.1'),
(50, 'Uloga: 127.0.0.1'),
(51, 'Uloga: 127.0.0.1'),
(52, 'Uloga: 127.0.0.1'),
(53, 'Uloga: 127.0.0.1'),
(54, 'Uloga: 127.0.0.1'),
(55, 'Uloga: 127.0.0.1'),
(56, 'Uloga: 127.0.0.1'),
(57, 'Uloga: 127.0.0.1'),
(58, 'Uloga: 127.0.0.1'),
(59, 'Uloga: 127.0.0.1'),
(60, 'Uloga: 127.0.0.1'),
(61, 'Uloga: 127.0.0.1'),
(62, 'Uloga: 127.0.0.1'),
(63, 'Uloga: 127.0.0.1'),
(64, 'Uloga: 127.0.0.1'),
(65, 'Uloga: 127.0.0.1'),
(66, 'Uloga: 127.0.0.1'),
(67, 'Uloga: 127.0.0.1'),
(68, 'Uloga: 127.0.0.1'),
(69, 'Uloga: 127.0.0.1'),
(70, 'Uloga: 127.0.0.1'),
(71, 'Uloga: 127.0.0.1'),
(72, 'Uloga: 127.0.0.1'),
(73, 'Uloga: 127.0.0.1'),
(74, 'Uloga: 127.0.0.1'),
(75, 'Uloga: 127.0.0.1'),
(76, 'Uloga: 127.0.0.1'),
(77, 'Uloga: 127.0.0.1'),
(78, 'Uloga: 127.0.0.1'),
(79, 'Uloga: 127.0.0.1'),
(80, 'Uloga: 127.0.0.1'),
(81, 'Uloga: 127.0.0.1'),
(82, 'Uloga: 127.0.0.1'),
(83, 'Uloga: 127.0.0.1'),
(84, 'Uloga: 127.0.0.1'),
(85, 'Uloga: 127.0.0.1'),
(86, 'Uloga: 127.0.0.1'),
(87, 'Uloga: 127.0.0.1'),
(88, 'Uloga: 127.0.0.1'),
(89, 'Uloga: 127.0.0.1'),
(90, 'Uloga: 127.0.0.1'),
(91, 'Uloga: 127.0.0.1'),
(92, 'Uloga: 127.0.0.1'),
(93, 'Uloga: 127.0.0.1'),
(94, 'Uloga: 127.0.0.1'),
(95, 'Uloga: 127.0.0.1'),
(96, 'Uloga: 127.0.0.1'),
(97, 'Uloga: 127.0.0.1'),
(98, 'Uloga: 127.0.0.1'),
(99, 'Uloga: 127.0.0.1'),
(100, 'Uloga: 127.0.0.1'),
(101, 'Uloga: 127.0.0.1'),
(102, 'Uloga: 127.0.0.1'),
(103, 'Uloga: 127.0.0.1'),
(104, 'Uloga: 127.0.0.1'),
(105, 'Uloga: 127.0.0.1'),
(106, 'Uloga: 127.0.0.1'),
(107, 'Uloga: 127.0.0.1'),
(108, 'Uloga: 127.0.0.1'),
(109, 'Uloga: 127.0.0.1'),
(110, 'Uloga: 127.0.0.1'),
(111, 'Uloga: 127.0.0.1'),
(112, 'Uloga: 127.0.0.1'),
(113, 'Uloga: 127.0.0.1'),
(114, 'Uloga: 127.0.0.1'),
(115, 'Uloga: 127.0.0.1'),
(116, 'Uloga: 127.0.0.1'),
(117, 'Uloga: 127.0.0.1'),
(118, 'Uloga: 127.0.0.1'),
(119, 'Uloga: 127.0.0.1'),
(120, 'Uloga: 127.0.0.1'),
(121, 'Uloga: 127.0.0.1'),
(122, 'Uloga: 127.0.0.1'),
(123, 'Uloga: 127.0.0.1'),
(124, 'Uloga: 127.0.0.1'),
(125, 'Uloga: 127.0.0.1'),
(126, 'Uloga: 127.0.0.1'),
(127, 'Uloga: 127.0.0.1'),
(128, 'Uloga: 127.0.0.1'),
(129, 'Uloga: 127.0.0.1'),
(130, 'Uloga: 127.0.0.1'),
(131, 'Uloga: 127.0.0.1'),
(132, 'Uloga: 127.0.0.1'),
(133, 'Uloga: 127.0.0.1'),
(134, 'Uloga: 127.0.0.1'),
(135, 'Uloga: 127.0.0.1'),
(136, 'Uloga: 127.0.0.1'),
(137, 'Uloga: 127.0.0.1'),
(138, 'Uloga: 127.0.0.1'),
(139, 'Uloga: 127.0.0.1'),
(140, 'Uloga: 127.0.0.1'),
(141, 'Uloga: 127.0.0.1'),
(142, 'Uloga: 127.0.0.1'),
(143, 'Uloga: 127.0.0.1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `naziv` (`naziv`),
  ADD UNIQUE KEY `slika` (`slika`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `korisnik`
--
ALTER TABLE `korisnik`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `korisnicko_ime` (`korisnicko_ime`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uloga`
--
ALTER TABLE `uloga`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(40) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `film`
--
ALTER TABLE `film`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `korisnik`
--
ALTER TABLE `korisnik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `uloga`
--
ALTER TABLE `uloga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
