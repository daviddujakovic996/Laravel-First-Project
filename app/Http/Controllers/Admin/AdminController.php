<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    function prikaziStatistiku(){
        echo "Statistike nema!";
    }

    function unesiStatistiku(Request $request) {
        // var_dump($_REQUEST);
        $ime = $request->input("ime");
        echo $ime;
    }
}
