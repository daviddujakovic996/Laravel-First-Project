<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Film;

class FilmController extends Controller
{
    public function getAll(){
        $model = new Film();
        $filmovi = $model->getAllNormalni();
        return view("filmovi", [
            "filmovi" => $filmovi
        ]);
    }
}
