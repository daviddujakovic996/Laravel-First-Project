<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontController extends Controller
{
    protected $data = [];

    public function  __construct()
    {
        // Ono sto vazi za sve stranice
        $this->data['navigacija'] = [
            [
                "id" => 1,
                "naslov" => "Home",
                "link" => url("/")
            ],
            [
                "id" => 2,
                "naslov" => "About",
                "link" => url("/about")
            ],
            [
                "id" => 3,
                "naslov" => "Contact",
                "link" => route("mojContact")
            ]
        ];
        $this->data['kategorije'] = [
            0 => [
                "id" => 1,
                "naslov" => "Kat 1",
                "link" => url("/kategorije/1")
            ],
            1 => [
                "id" => 2,
                "naslov"=> "Kat 2",
                "link" => url("/kategorije/2")
            ],

            2 => [
                "id" => 3,
                "naslov" => "Kategorija 3",
                "link" => url("/kategorije/3")
            ]
        ];
    }
}
