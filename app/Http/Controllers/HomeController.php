<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends FrontController
{
    public function index(){
        $proizvodi = [
            0 => [
                "naslov" => "Item 1",
                "opis" => "Nestoo..",
                "cena" => 45532.13
            ],
            1 => [
                "naslov" => "Item 2",
                "opis" => "Nestoo..",
                "cena" => 784.13
            ],
            2 => [
                "naslov" => "Item 3",
                "opis" => "Nestoo..",
                "cena" => 45532.13
            ],
            3 => [
                "naslov" => "Item 3",
                "opis" => "Nestoo..",
                "cena" => 45532.13
            ],
            4 => [
                "naslov" => "Item 3",
                "opis" => "Nestoo..",
                "cena" => 45532.13
            ]
        ];
        $this->data['proizvodi'] = $proizvodi;
        return view("pages.home", $this->data);
    }
}
