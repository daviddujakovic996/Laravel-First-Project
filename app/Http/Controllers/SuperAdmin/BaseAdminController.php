<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseAdminController extends Controller
{
    protected $data = [];

    public function __construct(){
        $this->data['navigacija'] = [
            0 => [
                "title" => "Unos proizvoda",
                "href" => "/admin/products/create"
            ],
            1 => [
                "title" => "Prikaz proizvoda",
                "href" => "#"
            ]
        ];
    }
}
