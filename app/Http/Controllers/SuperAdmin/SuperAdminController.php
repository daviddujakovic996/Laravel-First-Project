<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SuperAdminController extends BaseAdminController
{
    public function index(){
        return view("pages/admin/dashboard", $this->data);
    }
}
