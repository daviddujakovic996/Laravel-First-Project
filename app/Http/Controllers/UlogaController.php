<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Uloga;

class UlogaController extends Controller
{
    private $model;

    public function __construct(){
        $this->model = new Uloga();

        
    }


    public function getAll(){
        
        // if(!session()->has("user")){
        //     return \redirect("/login");
        // }

        // $model = new Uloga();    
        $uloge = $this->model->getAll();
        // var_dump($uloge);
        return view("tabelaUloge", [
            "uloge" => $uloge
        ]);
    }

    public function forma(){
        return view("form");
    }

    // WEB FORMA
    public function insert(Request $request){
        // $request -> ceo zahtev iz forme
        // dd($request);
        $naziv = $request->input("naziv");
        // var_dump($naziv);

        $id = $this->model->insert($naziv);
        // var_dump($id);

        // $redirect = redirect("/uloga");
        // dd($redirect);
        return redirect("/uloga")->with(["message" => "Uspeo unos!"]);
    }


    // AJAX 
    public function insertAjax(Request $request) {
        $naziv = $request->input("naziv");
        try {
            $id = $this->model->insert($naziv);
        }
        catch(\PDOException $ex){
            return response(["greska" => $ex->getMessage()], 505);
        }
        
        return ["id" => $id]; // STATUS CODE!!!

        // return redirect("/uloga"); // NIKAKO!!!
    }

    public function delete($id, $pera = null) {
        $this->model->delete($id);
        return response(null, 204);
    }

    public function getAllAjax(){
        $uloge = $this->model->getAll();
        return $uloge;        
    }
}
