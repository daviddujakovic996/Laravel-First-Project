<?php

namespace App\Http\Middleware;

use Closure;

class IpAdresa
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Storage -> Logs -> laravel.log
        \Log::info("Pristupljeno: ". date("Y-m-d H:i:s").$request->ip());

        // $neext -> iz WEB.PHP zna koji metod treba da se pozove
        
        // $next = "UlogaController@getAll"
        // return ....
        // "handle"($request);
        return $next($request);  
        // return redirect("google");

        // return redirect("/formaUloge");


        // dd($next);
        // return $next($request);
    }
}
