<?php

namespace App\Http\Middleware;

use Closure;

class IsLoggedIn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->session()->has("user"))
            return $next($request); // uloga@getAll
        else 
            return redirect("/login")->with("message", "MIDDLEWARE: Dalje neces moci!");
        
    }
}
