<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Uloga;

class UnosUloge
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $uloga = new Uloga();
        $uloga->insert("Uloga: " . $request->ip());
        return $next($request);
    }
}
