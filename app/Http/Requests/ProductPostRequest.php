<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // MORA DA BUDE TRUE
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        // redirect()->back()->with("errors"..)
        return [
            "title" => "required|alpha",
            "description" => "min:5",
            "price" => "required|numeric|min:0",
            "category" => "not_in:0",
            "images" => "required|mimes:jpeg,jpg,png|max:5000"
        ];
    }
}
