<?php

namespace App\Models;

class Film {
    public function getAll(){
        return \DB::select("SELECT * FROM film f INNER JOIN korisnik k ON f.korisnik_id = k.id");
    }

    public function getAllNormalni(){
        return \DB::table("film AS f")
                ->join("korisnik AS k", "f.korisnik_id", "=", "k.id")
                ->select("f.*","k.email")
                ->get(); // select("*") podrazumevano
    }
}