<?php

namespace App\Models;


class Image {
    
    public function insert($fileName, $idProizvod){
        return \DB::table("images")
            ->insert([
                "src" => $fileName,
                "productId" => $idProizvod
             ]);
    }
}