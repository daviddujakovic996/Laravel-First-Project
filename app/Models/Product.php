<?php

namespace App\Models;
use App\Http\Requests\ProductPostRequest;

class Product {
    private $table = "products";

    public function insert(ProductPostRequest $request){
        return \DB::table($this->table)
            ->insertGetId(
                [
                    "title" => $request->input("title"),
                    "description" => $request->input("description"),
                    "categoryId" => $request->input("category"),
                    "price" => $request->input("price")
                ]
            );
    }
}