<?php
namespace App\Models;
// use Illuminate\Support\Facades\DB;

class Uloga {
    public function getAll(){
        // $rezultat = DB::table("uloga")
        //         ->select("*")
        //         // ->where("...")
        //         // ->fetchAll();
        //         ->get();
        $rezultat = \DB::select("SELECT * FROM uloga");
        return $rezultat;
    }

    public function getAllDrugi(){
        return \DB::table("uloga")
                ->select("*")
                ->get();
    }

    public function insert($naziv){
        $insert = \DB::table("uloga")
                ->insertGetId([
                    "naziv" => $naziv
                ]); // INSERT INTO uloga (naziv) VALUES(...)
        return $insert; // ID unetog
        
    }

    public function delete($id){
        return \DB::table("uloga")
                ->where([
                    ["id", "=", $id]
                ])
                ->delete();
    }
}