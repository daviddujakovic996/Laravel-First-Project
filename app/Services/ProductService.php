<?php


namespace App\Services;
use App\Models\Category;
use App\Models\Product;
use App\Models\Image;
use App\Http\Requests\ProductPostRequest;

use App\Services\UploadService;

class ProductService {

    private $model;

    public function __construct(){
        $this->model = new Product();
    }

    public function insert($request){
        $nizSlika = $this->upload($request);

        \DB::beginTransaction();
        try {
            $idProizvod =  $this->model->insert($request);
            $this->insertImages($nizSlika, $idProizvod);
            \DB::commit();
        }
        catch(\Exception $ex) {
            \Log::error($ex->getMessage());
             \DB::rollback(); // vrati sve unazad!!!
        }
    }

    private function insertImages($nizSlika, $idProizvod){
        $imageModel = new Image();
        foreach($nizSlika as $slika){
            try {
                $imageModel->insert($slika, $idProizvod);
            } catch(\Exception $ex){
                \Log::error($ex->getMessage());
                // vrati sve unazad!!!
                \DB::rollback();
            }
        }
    }
    private function upload($request){
        $slika = $request->file("images");
        $nizSlika = [];

        // foreach($slike as $slika) {
            $nizSlika[] = UploadService::upload($slika); // naziv slike
        // }
        return $nizSlika;
    }
}