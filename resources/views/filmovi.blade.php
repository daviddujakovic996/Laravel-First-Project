<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <table border='1'>
        <tr>
            <th>ID</th>
            <th>Naziv</th>
            <th>Izmeni</th>
            <th>Obrisi</th>
        </tr>
        @foreach($filmovi as $film)
            <tr>
                <td>{{ $film->id }}</td>
                <td>{{ $film->naziv }}</td>
                <td><a>Izmeni</a></td>
                <td><a>Obrisi</a></td>
            </tr>
        @endforeach
        
    </table>
</body>
</html>