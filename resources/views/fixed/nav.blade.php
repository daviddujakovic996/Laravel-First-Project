<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">Start Bootstrap</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                    @foreach($navigacija as $link)
                            @component("partials.link", [
    "link" => $link
])
                                @endcomponent
                        @endforeach

{{--                <li class="nav-item active">--}}
{{--                    <a class="nav-link" href="{{ url("/") }}">Home--}}
{{--                        <span class="sr-only">(current)</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="nav-item">--}}
{{--                    <a class="nav-link" href="{{ url("/about") }}">About</a>--}}
{{--                </li>--}}
{{--                <li class="nav-item">--}}
{{--                    <a class="nav-link" href="#">Services</a>--}}
{{--                </li>--}}
{{--                <li class="nav-item">--}}
                    <!-- route() -> pristup po NAME rute -->
{{--                    <a class="nav-link" href="{{ route("mojContact") }}">Contact</a>--}}
{{--                </li>--}}


                    {{-- dd(session()->get("user")) --}}

                    @if(!session()->has('user'))
                    <li>
                        <a href="{{ url('/login') }}" class="nav-link">LOGIN</a>
                    </li>
                    @else 
                    <li>
                        <a href="{{ url('/uloga') }}" class="nav-link">Prikaz uloge</a>
                    </li> 
                    <li>
                        <a href="{{ url('/logout') }}" class="nav-link">LOGOUT</a>
                    </li>     
                    @endif
                    
            </ul>
        </div>
    </div>
</nav>
