<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-lg-3">

            <h1 class="my-4">Shop Name</h1>
            <ul class="list-group">
                @foreach($kategorije as $link)
                    @component("partials.link", [
                        "link" => $link
                    ])
                    @endcomponent
                @endforeach
            </ul>

        </div>
        <!-- /.col-lg-3 -->
