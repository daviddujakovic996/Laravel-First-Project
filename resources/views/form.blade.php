<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="{{ url('/unesiUlogu') }}" method="POST">
        <!-- {{ csrf_token() }}
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/> -->

        @csrf

        <input type="text" placeholder="Unesi naziv uloge" name="naziv" id="naziv"/>
        <input type="submit" name="btnSubmit" value="Unesi"/>
        <input type="button" name="dugme" value="AJAX Unesi" id="ajaxDugme"/>
    </form>


    <script src="{{ asset("/vendor/jquery/jquery.min.js")}}"></script>
    <script>
        $("#ajaxDugme").click(function(){
            $.ajax({
                url: "/unesiUloguAJAX",
                method: "POST",
                data: {
                    _token: $("input[name='_token']").val(),
                    naziv: $("#naziv").val()
                },
                dataType: "json",
                success: function(data){
                    console.log(data);
                },
                error: function(error){
                    console.log(error);
                    alert(error.responseJSON.greska);
                }
            })
        });
    
    </script>
</body>
</html>