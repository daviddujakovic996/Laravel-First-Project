@extends("layouts/admin")

@section("centar")
    <h3>Insert product</h3>

    Greske validacija:
    @isset($errors)
        @foreach($errors->all() as $poruka)
            {{ $poruka }} <br/>
        @endforeach
    @endisset

    <form action="{{ url('/admin/products') }}" method="POST" enctype="multipart/form-data">

        {{ csrf_field() }}
        
        <input type="text" name="title" placeholder="Title" class="form-control"/>
        <textarea name="description" class="form-control" placeholder="Description"></textarea>

        <input type="text" name="price" placeholder="Price" class="form-control"/>

        <select name="category" class="form-control">
            <option value="0">Choose category</option>
            @foreach($kategorije as $kategorija)
                <option value="{{ $kategorija->id }}">
                    {{ $kategorija->title }}
                </option>
            @endforeach

        </select>

        <label>Image</label>
        <input type="file" name="images" class="form-control"/>


        <input type="submit" name="btnSubmit"  value="Insert" class="form-control"/>
    </form>
@endsection