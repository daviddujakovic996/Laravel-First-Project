<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <a href="{{url('/formaUloge') }}">Dodaj ulogu</a>
    
    @if(session()->has("message"))
        {{ session("message") }}
    @endif
    
    <table border='1'>
        <tr>
            <th>ID</th>
            <th>Naziv</th>
            <th>Izmeni</th>
            <th>Obrisi</th>
        </tr>
        <tbody id="tbody">
        @foreach($uloge as $uloga)
            <tr>
                <td>{{ $uloga->id }}</td>
                <td>{{ $uloga->naziv }}</td>
                <td><a>Izmeni</a></td>
<td><a href="#" class="obrisi" data-id="{{ $uloga->id }}">Obrisi</a></td>
            </tr>
        @endforeach
        </tbody>
        

    </table>


    <script src="{{ asset("/vendor/jquery/jquery.min.js")}}"></script>
    <script>
        $(document).on("click", ".obrisi", function(){
            let id = $(this).data("id");
            $.ajax({
                url: "/api/obrisiUlogu/" + id,
                method: "DELETE",
                success: function(data){
                    console.log(data);
                    $.ajax({
                        url: "/api/uloga",
                        method: "GET",
                        success: function(uloge){
                            let html = "";

                            for(let uloga of uloge){
                                html += `<tr>
                                    <td>${uloga.id}</td>
                                    <td>${uloga.naziv}</td>
                                    <td><a>Izmeni</a></td>
                                    <td><a href="#" class="obrisi" data-id="${uloga.id}">Obrisi</a></td>
            
                                </tr>`;
                            }

                            $("#tbody").html(html);
                        }
                    })
                },
                error: function(error){
                    console.log(error);
                }
            })
        })
    </script>
</body>
</html>