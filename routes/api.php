<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// api/obrisiUlogu
// api/uloga

// NE TREBA CSRF!!!
Route::delete("/obrisiUlogu/{id}/{pera?}", "UlogaController@delete");

# ista kao u web.php
Route::get("/uloga", "UlogaController@getAllAjax");
