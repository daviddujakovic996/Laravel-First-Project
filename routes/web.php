<?php

# Auditorne 8
Route::prefix("/admin")->group(function(){

    // /admin/dasboard
    Route::get("/dashboard", "SuperAdmin\SuperAdminController@index");
    Route::resource("/products", "SuperAdmin\ProductsController");
});


# Auditorne 7
Route::get("/login", "AuthController@login");
Route::post("/login", "AuthController@doLogin");
Route::get("/logout", "AuthController@logout")->middleware(["isLoggedIn"]);

Route::prefix("/admin12")->middleware(["isLoggedIn", "admin"])-> group(function(){

    // GRUPA RUTA KOJA TREBA BITI ZASTICENA
    Route::get("/", function(){
        echo "Admin strana";
    });
    Route::get("/users", function(){ // admin/users
        echo "Admin rad sa users!";
    });
});

// POMOC -> generise rute
Route::resource("/users", "Admin\UserController");


# Auditorne 6 + 7 -> middleware added
Route::get("/uloga", "UlogaController@getAll")->middleware(["isLoggedIn"]);


Route::get("/formaUloge", "UlogaController@forma")->middleware(["isLoggedIn"]); // GLEDATI Kernel!!
Route::post("/unesiUlogu", "UlogaController@insert");

Route::post("/unesiUloguAJAX", "UlogaController@insertAjax");

# TRAZI CSRF
// Route::delete("/obrisiUlogu/{id}/{pera?}", "UlogaController@delete");



Route::get("/filmovi", "FilmController@getAll");

Route::get("/", "HomeController@index");
Route::get("/contact/contact/contact", "PagesController@contact")->name("mojContact");
Route::get("/about", "PagesController@about");

Route::get("/kategorije/{id?}", "PagesController@kategorije")
    ->where(["id"=> "\d+"]);

Route::redirect("/kat/{id?}", "/kategorije/{id?}");

Route::get("/proizvodi/{id}", function($id){

})->where(["id"=> "\d+"]);
